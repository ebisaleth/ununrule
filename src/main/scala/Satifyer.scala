
/* sounds like a sex toy. */
object SATifyer {


  def apply(board: Board): List[List[Int]] = {


    //fields that are known to be black or white.
    val fixed_fields: List[List[Variable]] =
      board.matrix.zipWithIndex.map { case (line, i) =>
        line.zipWithIndex.collect {
          case (Black, j) => List(Variable(i, j))
          case (White, j) => List(Variable(i, j, negated = true))
        }.toList
      }.toList.flatten

    // no three-clusters of the same colour in any ROW/COLUMN
    // creating clauses like {v_0,0, v_1,0, v_2,0}, {-v_0,0, -v_1,0, -v_2,0}
    def rule_of_threes: List[List[Variable]] = {
      (0 until board.n_of_rows).flatMap { r =>
        (0 until board.n_of_columns - 2).map { c =>
          List(
            List(Variable(r, c), Variable(r, c + 1), Variable(r, c + 2)),
            List(Variable(r, c, negated = true), Variable(r, c + 1, negated = true), Variable(r, c + 2, negated = true))
          )
        }
      } ++
        (0 until board.n_of_columns).flatMap { c =>
          (0 until board.n_of_rows - 2).map { r =>
            List(
              List(Variable(r, c), Variable(r + 1, c), Variable(r + 2, c)),
              List(Variable(r, c, negated = true), Variable(r + 1, c, negated = true), Variable(r + 2, c, negated = true))
            )
          }
        }
    }.flatten.toList

    /*
     * rows/columns must be split in half, i.e.
     * AND(v1, v2, v3, v4) => -v5 ===> -v1, -v2, -v3, -v4, -v5
     * AND(v1, v2, v3, v4) => -v6
     * AND(v1, v2, v3, v4) => -v7
     * AND(v1, v2, v3, v4) => -v8
     *
     *
     * */
    def rule_of_halfs = {
      (0 until board.n_of_rows).flatMap { r =>
        (0 until board.n_of_columns).combinations(board.n_of_columns / 2 + 1).toList.map { halfset =>
          List(halfset.toList.map { half => Variable(r, half) },
            halfset.toList.map { half => Variable(r, half, negated = true) })
        }
      } ++
        (0 until board.n_of_columns).flatMap { c =>
          (0 until board.n_of_rows).combinations(board.n_of_rows / 2 + 1).toList.map { halfset =>
            List(halfset.toList.map { half => Variable(half, c) },
              halfset.toList.map { half => Variable(half, c, negated = true) })
          }
        }
    }.flatten.toList

    val res = fixed_fields ++ rule_of_threes ++ rule_of_halfs

    res.map { clause =>
      clause.map {
        case Variable(c, r, true) => board.number_counter(c, r) * -1
        case Variable(c, r, false) => board.number_counter(c, r)
      }
    }
  }
}

// @formatter:off
case class Variable(row: Int, column: Int, negated: Boolean = false) {
  override def toString: String = s"${if (negated) {"-"} else {""}}v_$row,$column"
}
// @formatter:on

