

/* Sorry input parsing makes me v paranoid */
trait InputSquare

// @formatter:off
case object Black extends InputSquare {override def toString = "B"}
case object White extends InputSquare {override def toString = "W"}
case object Unkwn extends InputSquare {override def toString = "?"}
// @formatter:on
/*  _________width_______
*  /                     \
*
*  _,_,_,_,_,_,_,_,_,_,_,_ row  \          0,0 0,1 0,2 ...
*  _,_,_,_,_,_,_,_,_,*,_,_ row  |          1,0 1,1
*  _,                           | height   2,0
*  _,                           |               * = 1,9
*  _,                           |
*  column                      /
*
*  height = # of rows
*  width = # of columns
*
*  WHY AM I SO BAD AT THIS LOL
*  I HAD TO SWITCH THEM UP AT LEAST 3 TIMES OMG
*
* */
case class Board(matrix: Vector[Vector[InputSquare]]) {
  val n_of_columns: Int = matrix.head.length // HEIGHT
  val n_of_rows: Int = matrix.length // WIDTH

  //sanity check
  if (matrix.exists(_.length != matrix.head.length)) {
    println("Your unruly field is invalid, please consult a local oracle machine.")
    System.exit(-1)
  }

  def number_counter(r: Int, c: Int): Int = {
    r * n_of_columns + c + 1
  }

}