/*
*
* This is ununrule. it solves unruly puzzles
* SAT-solving project by e-beth 18ws19
* enjoy my terrible code
*
* */

import java.io.{BufferedWriter, File, FileWriter}
import scala.sys.process._
import scala.io.Source
import scala.util.{Failure, Success, Try}

import scalafx.application.JFXApp


object Main extends JFXApp {

  /* Let there be fun(ctions) */

  def promptInputAndReadFile(): List[String] = {
    val filename = "/home/elisabeth/Dropbox/#Uni/19ws/sat/u01/u01puzzle-small1.txt"
    //val filename = readLine("Please tell me the location of your very unruly puzzle: ")
    Try(Source.fromFile(filename).getLines.toList) match {
      case Success(lines) => lines
      case Failure(f) => {
        println("Sorry, that didn't work. Java says: " + f + "\nPlease try again!"); promptInputAndReadFile()
      }
    }
  }

  def processLines(lines: List[String]): Vector[Vector[InputSquare]] = {
    lines
      .tail
      .toVector
      .map(line => line.map {
        case 'W' => White
        case 'B' => Black
        case '?' => Unkwn
        case chr =>
          throw new Exception(s"I found a weird character in your field. It looks like this: $chr. Please consult your local oracle machine.")
      }.toVector)
  }

  def parsesolution(list: List[String]) = {
    val vars = list.flatMap(line => line.split(" ").toList.tail).map(_.toInt)
    vars
  }


  /* Here, stuff happens. */
  println("Hello, this is ununrule")

  val lines = promptInputAndReadFile()

  println("I found the following puzzle: ")
  println(lines.tail.mkString("\n"))

  /* Create the Board */
  val board = Board(processLines(lines))

  /* Try to solve the Board */

  val OUTPUTFILEPATH = "CNF"

  val outputfile = new File(OUTPUTFILEPATH)
  val bw = new BufferedWriter(new FileWriter(outputfile))
  Try {
    bw.write("p cnf 1 2\n" + SATifyer(board).map(clause => clause.mkString(" ")).mkString(" 0\n") + " 0")
  } match {
    case Success(value) => println("wrote CNF to file: " + OUTPUTFILEPATH + "...")
    case Failure(exception) => println("something went wrong: " + exception)
  }
  bw.close()

  println("asking picosat to solve...")
  val picosolved = s"./picosat-965/picosat -f $OUTPUTFILEPATH".lineStream_!.toList

  /* Parsing pico's output */

  picosolved.head.trim match {
    case "s SATISFIABLE" =>
      println("picosat found a solution! :)\nparsing pico-output...")
    case "s UNSATISFIABLE" =>
      println("sorry, picosat says your puzzle can't be solved :(")
      System.exit(-1)
    case something =>
      println("sorry, something went wrong along the way: " + something)
      System.exit(-1)
  }

  val vars: List[Int] = parsesolution(picosolved.tail)

  Renderer(vars, board)



}
