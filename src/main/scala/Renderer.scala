import Main.stage
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafx.scene.paint.Color._
import scalafx.scene.shape.Rectangle

object Renderer {

  def apply(l: List[Int], board: Board): Unit = {

    val field: List[List[InputSquare]] = l
      .grouped(board.n_of_columns)
      .toList
      .init
      .map { line =>
        line.map { n =>
          if (n < 0) {
            White
          } else {
            Black
          }
        }
      }

    // string output
    println(field.map(_.mkString("")).mkString("\n"))


    // graphical output
    stage = new JFXApp.PrimaryStage {
      width = board.n_of_columns * 32
      height = board.n_of_rows * 32 + 38 // weird header id even k
      scene = new Scene {
        fill = SlateGrey
        content = {
          field.zipWithIndex.flatMap { case (line,i) =>
            line.zipWithIndex.map { case (square,j) =>
              new Rectangle{
                x = j*32
                y = i*32
                width = 32
                height = 32
                fill = square match {
                  case Black => DarkSlateGrey
                  case White => BlanchedAlmond
                  case Unkwn => SlateGrey
                }
              }
            }
          }
        }
      }
    }
  }
}
